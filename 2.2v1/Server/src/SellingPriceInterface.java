import java.rmi.*;
import java.rmi.server.*;   

public interface SellingPriceInterface extends Remote{

	public double sellingPrice(double price,int year) throws RemoteException;
}
