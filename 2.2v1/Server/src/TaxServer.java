import java.rmi.*;
import java.rmi.server.*;   

public class TaxServer {

	public static void main (String[] argv) {
		   try {
			   Tax Hello = new Tax();			   		   
			   Naming.rebind("rmi://localhost/ABC", Hello);

			   System.out.println("Tax Server is ready.");
			   }catch (Exception e) {
				   System.out.println("Tax Server failed: " + e);
				}
		   }
}
