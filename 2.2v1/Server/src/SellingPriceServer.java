import java.rmi.*;
import java.rmi.server.*;   

public class SellingPriceServer {

	public static void main (String[] argv) {
		   try {
			   SellingPrice Hello = new SellingPrice();			   		   
			   Naming.rebind("rmi://localhost/ABC", Hello);

			   System.out.println("SellingPrice Server is ready.");
			   }catch (Exception e) {
				   System.out.println("SellingPrice Server failed: " + e);
				}
		   }
}
