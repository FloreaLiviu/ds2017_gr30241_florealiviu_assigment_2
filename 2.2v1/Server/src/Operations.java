import java.rmi.*;
import java.rmi.server.*;

public class Operations extends UnicastRemoteObject

	implements OperationsInterface {
	
	public Operations () throws RemoteException {   }
	
	public double sellingPrice(double a, int b) throws RemoteException {
		   double result = a - ((double)a / 7) * (2015 - b);
		   return result;
		}

	public double tax(int a, int b) throws RemoteException {
	  	  double result = ((double)a/200)*b;
	  	  return result;
	    }
}
