import java.rmi.*;
import java.rmi.server.*;

public class Tax extends UnicastRemoteObject 

	implements TaxInterface {
	
	public Tax () throws RemoteException {   }
	
	public double tax(int a, int b) throws RemoteException {
  	  double result = ((double)a/200)*b;
  	  return result;
    }
}