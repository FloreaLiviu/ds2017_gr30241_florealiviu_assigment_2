import java.rmi.*;
 
   public interface TaxInterface extends Remote {
	   
	   public double tax(int engineSize,int sum) throws RemoteException;
   }