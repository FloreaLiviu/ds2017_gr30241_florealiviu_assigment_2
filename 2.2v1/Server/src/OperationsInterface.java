import java.rmi.*;
import java.rmi.server.*;   

public interface OperationsInterface extends Remote{

	public double sellingPrice(double price,int year) throws RemoteException;
	public double tax(int engineSize,int sum) throws RemoteException;

}
