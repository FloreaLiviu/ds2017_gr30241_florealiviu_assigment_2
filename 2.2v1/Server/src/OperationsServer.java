import java.rmi.*;
import java.rmi.server.*;   

public class OperationsServer {

	public static void main (String[] argv) {
		   try {
			   Operations Hello = new Operations();			   		   
			   Naming.rebind("rmi://localhost/ABC", Hello);

			   System.out.println("Operations Server is ready.");
			   }catch (Exception e) {
				   System.out.println("Operations Server failed: " + e);
				}
		   }
}
