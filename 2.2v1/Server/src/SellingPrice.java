import java.rmi.*;
import java.rmi.server.*;

public class SellingPrice extends UnicastRemoteObject

	implements SellingPriceInterface {
		
	public SellingPrice () throws RemoteException {   }
		
	public double sellingPrice(double a, int b) throws RemoteException {
	   double result = a - ((double)a / 7) * (2015 - b);
	   return result;
	}
}
